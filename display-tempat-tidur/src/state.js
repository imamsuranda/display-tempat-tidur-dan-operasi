import { atom, selector } from "recoil";

export const recoilDataRuangan = atom({
  key: "recoilDataRuangan",
  default: [], // pada useState, ini sama seperti initail atau default value
});
