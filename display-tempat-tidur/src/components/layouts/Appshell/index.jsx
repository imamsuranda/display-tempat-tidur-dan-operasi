import React from "react";
import Navbar from "../Navbar";
const Appshell = (props) => {
  const { children } = props;
  return (
    <>
      <div className="w-full mt-10 border-b-2 border-black">
        <Navbar className="w-full" />
      </div>
      {children}
    </>
  );
};

export default Appshell;
