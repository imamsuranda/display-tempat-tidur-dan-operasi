import React from "react";

const Card = ({ data }) => {
  return (
    <section>
      <div className="flex flex-col gap-5 w-full m-3 ">
        {data && data.bpjs ? (
          <div className="grid grid-cols-2 gap-5 ">
            {data.bpjs.map((item, index) => (
              <div
                key={index}
                className="bg-clip-border border border-gray-200 rounded-lg shadow-md "
              >
                <div className="grid grid-cols-4 bg-white rounded-lg">
                  <div className="col-span-1 bg-green-800 rounded-lg h-full flex justify-center items-center uppercase font-semibold text-white">
                    <div className="text-3xl p-3">{item.namakelas}</div>
                  </div>
                  <div className="col-span-1 p-5 flex items-center justify-center flex-col border-r capitalize">
                    <div className="text-3xl font-semibold">Total kamar</div>
                    <div className="text-5xl font-bold">{item.kapasitas}</div>
                  </div>
                  <div className="col-span-1 p-5 flex items-center justify-center flex-col border-r capitalize">
                    <div className="text-3xl font-semibold">Terisi</div>
                    <div className="text-5xl font-bold">
                      {parseInt(item.kapasitas - item.tersedia)}
                    </div>
                  </div>
                  <div className="col-span-1 p-5 flex items-center justify-center flex-col capitalize">
                    <div className="text-3xl font-semibold">Tersedia</div>
                    <div className="text-5xl font-bold">{item.tersedia}</div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        ) : (
          <div className="flex justify-center items-center">Data kosong</div>
        )}
      </div>
    </section>
  );
};

export default Card;
