import React from "react";
import DisplayRuanganPage from "./pages/displayRuangan/index.jsx";

const App = () => {
  return (
    <div className="App flex justify-center items-center">
      <DisplayRuanganPage />
    </div>
  );
};

export default App;
