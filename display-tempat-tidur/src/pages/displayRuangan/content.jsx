import React, { useEffect } from "react";
import { useRecoilState } from "recoil";
import { recoilDataRuangan } from "../../state";
import Card from "../../components/elements/Card";
import axios from "axios";

const Content = () => {
  const [data, setData] = useRecoilState(recoilDataRuangan);

  useEffect(() => {
    // URL API dan token
    const apiUrl = "http://192.168.101.101:3005/keuanganX/ruangan/bpjs";
    const token = import.meta.env.VITE_APP_TOKEN;

    // Set headers dengan token
    const headers = {
      token: `${token}`,
    };

    // Lakukan request GET dengan Axios
    axios
      .get(apiUrl, { headers })
      .then((response) => {
        // Set data ke state recoil
        setData(response.data);
      })
      .catch((error) => {
        // Handle error di sini
        console.error("Error fetching data:", error);
      });
  }, []); // [] agar useEffect dijalankan hanya sekali saat komponen di-mount

  return (
    <div className="">
      <Card data={data} />
    </div>
  );
};

export default Content;
