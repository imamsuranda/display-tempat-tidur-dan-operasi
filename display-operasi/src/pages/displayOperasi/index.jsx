import React from "react";
import Content from "./content";

const index = () => {
  return (
    <div className="mt-10 w-full overflow-hidden">
      <Content />
    </div>
  );
};

export default index;
