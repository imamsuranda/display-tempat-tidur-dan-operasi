import React, { useEffect } from "react";
import { useRecoilState } from "recoil";
import { recoilDataOperasi } from "../../state";
import axios from "axios";
import TableData from "../../components/elements/TableData";
const Content = () => {
  const [data, setData] = useRecoilState(recoilDataOperasi);

  useEffect(() => {
    // URL API dan token
    const tanggaloperasi = new Date().toISOString().slice(0, 10);
    const apiUrl = `http://192.168.101.101:3005/bpjs/op?tanggaloperasikedepan=${tanggaloperasi}`;
    const token = import.meta.env.VITE_APP_TOKEN;

    // Set headers dengan token
    const headers = {
      token: `${token}`,
    };

    // Lakukan request GET dengan Axios
    axios
      .get(apiUrl, { headers })
      .then((response) => {
        // Set data ke state recoil
        setData(response.data);
      })
      .catch((error) => {
        // Handle error di sini
        console.error("Error fetching data:", error);
      });
  }, []); // [] agar useEffect dijalankan hanya sekali saat komponen di-mount

  return (
    <div className="">
      <TableData data={data} />
    </div>
  );
};

export default Content;
