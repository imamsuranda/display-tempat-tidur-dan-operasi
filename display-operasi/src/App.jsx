import React from "react";
import DisplayOperasiPage from "../src/pages/displayOperasi/index.jsx";
const App = () => {
  return (
    <div className="App">
      <DisplayOperasiPage />
    </div>
  );
};

export default App;
