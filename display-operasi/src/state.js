import { atom, selector } from "recoil";

export const recoilDataOperasi = atom({
  key: "recoilDataOperasi",
  default: [], // pada useState, ini sama seperti initail atau default value
});
