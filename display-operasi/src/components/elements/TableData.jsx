import React from "react";
import { Table } from "antd";

const columnsRacikan = [
  {
    title: "Tanggal Operasi",
    dataIndex: "tanggaloperasi",
  },
  {
    title: "No. Operasi",
    dataIndex: "kodebooking",
  },
  {
    title: "Tindakan",
    dataIndex: "jenistindakan",
  },
  {
    title: "Nama Poli",
    dataIndex: "namapoli",
  },
];

const TableData = (props) => {
  const { data } = props;

  const dataOperasi = data.map((item, index) => ({
    key: index.toString(),
    ...item,
  }));

  if (data) {
    console.log(data);
  } else {
    console.log("data kosong");
  }

  const customColumns = columnsRacikan.map((column) => ({
    ...column,
    style: { fontSize: "32px", fontWeight: "bold" },
  }));

  return (
    <div className="border border-gray-300 p">
      <Table
        columns={customColumns}
        dataSource={dataOperasi}
        pagination={false}
        bordered={true}
        size="small"
        components={{
          body: {
            row: (props) => (
              <tr
                className="font-medium text-center"
                style={{ fontSize: "30px", fontFamily: "Poppins" }}
              >
                {props.children}
              </tr>
            ),
          },
          header: {
            cell: (props) => (
              <th
                style={{
                  fontSize: "30px",
                  fontWeight: "bold",
                  textAlign: "center",
                  fontFamily: "Poppins",
                }}
              >
                {props.children}
              </th>
            ),
          },
        }}
      />
    </div>
  );
};

export default TableData;
