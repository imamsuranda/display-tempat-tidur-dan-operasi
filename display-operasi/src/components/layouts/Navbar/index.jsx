import React from "react";

const Navbar = () => {
  return (
    <>
      <div className=" w-full ml-10">
        <div className="flex gap-x-52">
          <img width={300} src="./logo-baru-rspur.png" />
          <div className="text-4xl flex items-center font-semibold lg:ml-56 md:ml-0">
            Jadwal Tindakan Operasi
          </div>
        </div>

        {/* <Image>ini Gambar</Image> */}
      </div>
    </>
  );
};

export default Navbar;
