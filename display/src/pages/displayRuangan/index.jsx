import React from "react";
import Content from "./Content";
const index = () => {
  return (
    <div className="mt-5  overflow-hidden">
      <Content />
    </div>
  );
};

export default index;
