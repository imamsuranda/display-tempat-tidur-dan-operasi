import React, { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import { recoilDataRuangan, lastUpdateState } from "../../state";
import Card from "../../components/elements/Card";
import axios from "axios";

const Content = () => {
  const [data, setData] = useRecoilState(recoilDataRuangan);
  const [lastUpdate, setLastUpdate] = useRecoilState(lastUpdateState);
  const [fetchCounter, setFetchCounter] = useState(1);

  useEffect(() => {
    const fetchData = async () => {
      let date = new Date();
      let tahun = date.getFullYear(),
        bulan = date.getMonth(),
        tgl = date.getDate(),
        jam = date.getHours(),
        menit = date.getMinutes(),
        detik = date.getSeconds();
      let formattedMenit = String(menit).padStart(2, "0");
      const monthsNameInId = [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember",
      ];
      let monthName = monthsNameInId[bulan];

      const result =
        tgl +
        "  " +
        monthName +
        " " +
        tahun +
        " / " +
        jam +
        ":" +
        formattedMenit +
        " WIB";

      try {
        const apiUrl = "http://192.168.101.101:3005/keuanganX/ruangan/bpjs";
        const token = import.meta.env.VITE_APP_TOKEN;

        const headers = {
          token: `${token}`,
        };

        const response = await axios.get(apiUrl, { headers });
        setData(response.data);
        // Set waktu terakhir pembaharuan di Recoil state
        setLastUpdate(result);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    // Panggil fetchData untuk pertama kali
    fetchData();

    // Atur interval untuk memanggil fetchData setiap satu menit
    const intervalId = setInterval(() => {
      console.log(`Fetching data ${fetchCounter}`);
      fetchData();
      console.log(data.bpjs);
      setFetchCounter((prevCounter) => prevCounter + 1);
    }, 60000); // 60000 milidetik = 1 menit

    // Bersihkan interval saat komponen di-unmount
    return () => clearInterval(intervalId);
  }, [setData, fetchCounter]);

  return (
    <div className="">
      <Card data={data} />
    </div>
  );
};

export default Content;
