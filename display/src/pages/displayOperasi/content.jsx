import React, { useEffect } from "react";
import { useRecoilState } from "recoil";
import { recoilDataOperasi, lastUpdateState } from "../../state";
import axios from "axios";
import TableData from "../../components/elements/TableData";
const Content = () => {
  const [data, setData] = useRecoilState(recoilDataOperasi);
  const [lastUpdate, setLastUpdate] = useRecoilState(lastUpdateState);

  useEffect(() => {
    let count = 1;
    const fetchData = async () => {
      let date = new Date();
      let tahun = date.getFullYear(),
        bulan = date.getMonth(),
        tgl = date.getDate(),
        jam = date.getHours(),
        menit = date.getMinutes(),
        detik = date.getSeconds();
      let formattedMenit = String(menit).padStart(2, "0");
      const monthsNameInId = [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember",
      ];
      let monthName = monthsNameInId[bulan];

      const result =
        tgl +
        "  " +
        monthName +
        " " +
        tahun +
        " / " +
        jam +
        ":" +
        formattedMenit +
        " WIB";

      try {
        const tanggaloperasi = new Date().toISOString().slice(0, 10);
        const apiUrl = `http://192.168.101.101:3005/bpjs/op?tanggaloperasikedepan=${tanggaloperasi}`;
        const token = import.meta.env.VITE_APP_TOKEN;

        const headers = {
          token: `${token}`,
        };

        const response = await axios.get(apiUrl, { headers });
        setData(response.data);
        setLastUpdate(result);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    // Panggil fetchData untuk pertama kali
    fetchData();

    // Atur interval untuk memanggil fetchData setiap satu menit
    const intervalId = setInterval(() => {
      console.log(`Fetching data ${count++}`);
      fetchData();
    }, 60000); // 60000 milidetik = 1 menit

    // Bersihkan interval saat komponen di-unmount
    return () => clearInterval(intervalId);
  }, [setData]);

  return (
    <div className="">
      <TableData data={data} />
    </div>
  );
};

export default Content;
