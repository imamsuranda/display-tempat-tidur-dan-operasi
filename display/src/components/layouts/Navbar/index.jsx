import React, { useEffect, useState } from "react";
import Logo from "/logo-baru-rspur.png";
import { useRecoilValue } from "recoil";
import { lastUpdateState } from "../../../state";

const Navbar = () => {
  const [navbarTitle, setNavbarTitle] = useState("");
  const lastUpdate = useRecoilValue(lastUpdateState);

  const handleHashChange = () => {
    const currentHash = window.location.hash;

    if (currentHash === "#/display/ruangan/operasi") {
      setNavbarTitle("Jadwal Tindakan Operasi");
    } else if (currentHash === "#/display/ruangan/perawatan") {
      setNavbarTitle("Ketersediaan Ruang Perawatan");
    } else {
      setNavbarTitle("Default Title");
    }
  };

  useEffect(() => {
    // Panggil handleHashChange saat komponen di-mount
    handleHashChange();

    // Tambahkan event listener untuk mendengarkan perubahan hash
    window.addEventListener("hashchange", handleHashChange);
  }, []); // Dependensi kosong agar useEffect hanya dipanggil sekali saat mounting

  return (
    <>
      <div className="w-full h-full  px-10">
        <div className="flex justify-between ">
          <img width={300} src={Logo} />
          <div className="text-4xl flex items-center font-semibold">
            {navbarTitle}
          </div>
          <div className="text-1xl  font-semibold flex flex-col justify-center pr-1">
            <p>Last Update</p>
            <p>{lastUpdate}</p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
