import React from "react";

const Card = ({ data }) => {
  // Fungsi untuk mendapatkan nomor urut dari nama kelas
  const getClassOrder = (kelas) => {
    // Tentukan urutan kelas yang diinginkan untuk kolom kiri dan kanan
    const orderLeft = { NON: 1, KL1: 2, KL2: 3, KL3: 4 };
    const orderRight = { VIP: 1, ICU: 2, PICU: 3, NICU: 4 };

    // Ambil urutan dari objek order atau berikan nilai default Infinity jika tidak ditemukan
    return orderLeft[kelas] || orderRight[kelas] || Infinity;
  };

  // Urutkan data.bpjs berdasarkan nomor urut kelas
  const sortedData =
    data && data.bpjs
      ? [...data.bpjs].sort(
          (a, b) => getClassOrder(a.kodekelas) - getClassOrder(b.kodekelas)
        )
      : [];

  // Pisahkan data menjadi dua array berdasarkan kelas (kiri dan kanan)
  const leftColumnData = sortedData.filter(
    (item) => getClassOrder(item.namakelas) <= 4
  );
  const rightColumnData = sortedData.filter(
    (item) => getClassOrder(item.namakelas) > 4
  );

  return (
    <section className="flex justify-center max-w-full-full">
      <div className="flex gap-5 w-full">
        {sortedData.length > 0 ? (
          <div className="grid grid-cols-2 gap-5">
            <div className="col">
              {rightColumnData.map((item, index) => (
                <div
                  key={index}
                  className="bg-clip-border border border-gray-200 rounded-lg shadow-md "
                >
                  <div className="grid grid-cols-4 bg-white rounded-lg">
                    <div className="col-span-1 bg-green-800 rounded-lg h-full flex justify-center items-center uppercase font-semibold text-white">
                      <div className="text-3xl p-3">{item.namakelas}</div>
                    </div>
                    <div className="col-span-1 p-5 flex items-center justify-center flex-col border-r capitalize">
                      <div className="md:text-sm lg:text-3xl font-bold md:text-center ">
                        Total kamar
                      </div>
                      <div className="text-5xl font-bold">{item.kapasitas}</div>
                    </div>
                    <div className="col-span-1 p-5 flex items-center justify-center flex-col border-r capitalize">
                      <div className="text-3xl font-semibold">Terisi</div>
                      <div className="text-5xl font-bold">
                        {parseInt(item.kapasitas - item.tersedia)}
                      </div>
                    </div>
                    <div className="col-span-1 p-5 flex items-center justify-center flex-col capitalize">
                      <div className="text-3xl font-semibold">Tersedia</div>
                      <div className="text-5xl font-bold">{item.tersedia}</div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div className="col">
              {leftColumnData.map((item, index) => (
                <div
                  key={index}
                  className="bg-clip-border border border-gray-200 rounded-lg shadow-md "
                >
                  <div className="grid grid-cols-4 bg-white rounded-lg">
                    <div className="col-span-1 bg-green-800 rounded-lg h-full flex justify-center items-center uppercase font-semibold text-white">
                      <div className="text-3xl p-3">{item.namakelas}</div>
                    </div>
                    <div className="col-span-1 p-5 flex items-center justify-center flex-col border-r capitalize">
                      <div className="md:text-sm lg:text-3xl font-bold md:text-center">
                        Total kamar
                      </div>
                      <div className="text-5xl font-bold">{item.kapasitas}</div>
                    </div>
                    <div className="col-span-1 p-5 flex items-center justify-center flex-col border-r capitalize">
                      <div className="text-3xl font-semibold">Terisi</div>
                      <div className="text-5xl font-bold">
                        {parseInt(item.kapasitas - item.tersedia)}
                      </div>
                    </div>
                    <div className="col-span-1 p-5 flex items-center justify-center flex-col capitalize">
                      <div className="text-3xl font-semibold">Tersedia</div>
                      <div className="text-5xl font-bold">{item.tersedia}</div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ) : (
          <div className="flex justify-center items-center">Data kosong</div>
        )}
      </div>
    </section>
  );
};

export default Card;
