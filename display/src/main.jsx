import React from "react";
import ReactDOM from "react-dom/client";

import App from "./App.jsx";
import "./index.css";
import { RecoilRoot } from "recoil"; // panggil paket recoil yg telah di install
import Appshell from "./components/layouts/Appshell";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RecoilRoot>
      <Appshell>
        <App />
      </Appshell>
    </RecoilRoot>
  </React.StrictMode>
);
