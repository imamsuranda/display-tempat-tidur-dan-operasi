import React from "react";
import {
  HashRouter as Router, // Ganti BrowserRouter menjadi HashRouter
  Route,
  Routes,
  Navigate,
} from "react-router-dom";

import DisplayRuanganPage from "./pages/displayRuangan/index.jsx";
import DisplayOperasiPage from "./pages/displayOperasi/index.jsx";

const App = () => {
  return (
    <div className="App flex justify-center items-center">
      <Router>
        <Routes>
          <Route
            path="/"
            element={<Navigate to="/display/ruangan/perawatan" />}
          />
          <Route
            path="/display/ruangan/perawatan"
            element={<DisplayRuanganPage />}
          />
          <Route
            path="/display/ruangan/operasi"
            element={<DisplayOperasiPage />}
          />
        </Routes>
      </Router>
    </div>
  );
};

export default App;
