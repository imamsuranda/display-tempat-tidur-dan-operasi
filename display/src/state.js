import { atom, selector } from "recoil";

export const recoilDataRuangan = atom({
  key: "recoilDataRuangan",
  default: [], // pada useState, ini sama seperti initail atau default value
});

export const recoilDataOperasi = atom({
  key: "recoilDataOperasi",
  default: [], // pada useState, ini sama seperti initail atau default value
});

export const lastUpdateState = atom({
  key: "lastUpdateState",
  default: null,
});
